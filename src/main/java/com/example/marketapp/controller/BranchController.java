package com.example.marketapp.controller;
import com.example.marketapp.dto.BranchDto;
import com.example.marketapp.service.BranchService;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("branch")

public class BranchController {

    private BranchService branchService;

    public BranchesController(BranchService branchService) {
        this.branchService = branchService;
    }
    @PostMapping
    public void save(BranchDto branchDto){
        branchService.save(branchDto);
    }

    @PutMapping
    public void update(@RequestBody BranchDto branchDto){
        branchService.update(branchDto);
    }

    @GetMapping("/{id}")
    public BranchDto get(@PathVariable Integer id){
        return branchService.get(id);
    }

    @DeleteMapping("/id")
    public void delete(@PathVariable Integer id){
        branchService.delete(id);
    }


}
