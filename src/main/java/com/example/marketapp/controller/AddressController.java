package com.example.marketapp.controller;
import com.example.marketapp.dto.AddressDto;
import com.example.marketapp.service.AddressService;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/adres")
public class AddressController {

    private AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }
    @PostMapping
    public void save(AddressDto addressDto){
        addressService.save(addressDto);
    }
    @PutMapping
    public void update(@RequestBody AddressDto addressDto){
        addressService.update(addressDto);
    }
    @GetMapping("/{id}")
    public AddressDto get(@PathVariable Integer id){
        return addressService.get(id);
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        addressService.delete(id);
    }
}

