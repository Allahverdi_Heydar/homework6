package com.example.marketapp.controller;
import com.example.marketapp.dto.MarketDto;
import com.example.marketapp.service.MarketService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/markets")
public class MarketController {

    private MarketService marketService;

    public MarketController(MarketService marketService) {
        this.marketService = marketService;
    }
    @PostMapping
    public void save(@RequestBody MarketDto marketDto){
        marketService.save(marketDto);

    }
    @PutMapping
    public void update(@RequestBody MarketDto marketDto){
        marketService.update(marketDto);

    }
    @GetMapping("/{id}")
    public MarketDto get(@PathVariable Integer id){
        return marketService.get(id);
    }
    @GetMapping
    public List<MarketDto> getAll(){
        return marketService.getAll();

    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        marketService.delete(id);
    }

}
