package com.example.marketapp.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class AddressDto {
    private  Integer id;
    private  String name;
}
