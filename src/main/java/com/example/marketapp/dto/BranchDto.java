package com.example.marketapp.dto;

import com.example.marketapp.entity.Address;
import lombok.Data;

@Data
public class BranchDto {
    private Integer id;
    private String name;

    private Address address;
}
