package com.example.marketapp.dto;

import com.example.marketapp.entity.Branch;
import lombok.Data;

@Data
public class MarketDto {

    private Integer id;
    private String name;
    private Branch branch;


}
