package com.example.marketapp.service;

import com.example.marketapp.dto.BranchDto;
import com.example.marketapp.entity.Branch;
import com.example.marketapp.repository.BranchRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BranchService {
    private BranchRepository branchRepository;
    private ModelMapper modelMapper;

    public BranchService(BranchRepository branchRepository, ModelMapper modelMapper) {
        this.branchRepository = branchRepository;
        this.modelMapper = modelMapper;
    }

    public void save(BranchDto branchDto) {

        Branch branch = modelMapper.map(branchDto, Branch.class);
        branchRepository.save(branch);
    }

    public void update(BranchDto branchDto) {
        Optional<Branch> entity = branchRepository.findById(branchDto.getId());
        entity.ifPresent(branches -> {
            if (branchDto.getName() != null) {
                branches.setName(branchDto.getName());
            }
            branchRepository.save(branches);
        });
    }

    public BranchDto get(Integer id) {
        Branch branch = branchRepository.findById(id).get();
        BranchDto branchDto = modelMapper.map(branch, BranchDto.class);
        return branchDto;

    }

    public void delete(Integer id) {

        branchRepository.deleteById(id);
    }
}
