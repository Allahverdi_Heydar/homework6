package com.example.marketapp.service;

import com.example.marketapp.dto.AddressDto;
import com.example.marketapp.entity.Address;
import com.example.marketapp.repository.AddressRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressService {
    private AddressRepository addressRepository;
    private ModelMapper modelMapper;

    public AddressService(AddressRepository addressRepository, ModelMapper modelMapper) {
        this.addressRepository = addressRepository;
        this.modelMapper = modelMapper;
    }

    public void save(AddressDto addressDto) {
        Address address = modelMapper.map(addressDto,Address.class);
        addressRepository.save(address);

    }

    public void update(AddressDto addressDto) {
        Optional<Address> entity = addressRepository.findById(addressDto.getId());
        entity.ifPresent(address -> {
            if (addressDto.getName()!=null){
                address.setName(addressDto.getName());
            }
            addressRepository.save(address);
        });
    }

    public AddressDto get(Integer id) {
        Address address = addressRepository.findById(id).get();
        AddressDto addressDto= modelMapper.map(address,AddressDto.class);
        return addressDto;
    }

    public void delete(Integer id) {
        addressRepository.deleteById(id);
    }
}

