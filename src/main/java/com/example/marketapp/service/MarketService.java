package com.example.marketapp.service;

import com.example.marketapp.dto.MarketDto;
import com.example.marketapp.entity.Market;
import com.example.marketapp.repository.MarketRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MarketService {
    private MarketRepository marketRepository;
    private ModelMapper modelMapper;

    public MarketService(MarketRepository marketRepository, ModelMapper modelMapper) {
        this.marketRepository = marketRepository;
        this.modelMapper = modelMapper;
    }

    public void save(MarketDto marketDto) {

        Market market = modelMapper.map(marketDto, Market.class);
        marketRepository.save(market);
    }

    public void update(MarketDto marketDto) {
        Optional<Market> entity = marketRepository.findById(marketDto.getId());
        entity.ifPresent(market -> {
            if (marketDto.getName()!=null){
                market.setName(marketDto.getName());
            }
            marketRepository.save(market);
        });
    }

    public MarketDto get(Integer id) {
        Market market = marketRepository.findById(id).get();
        MarketDto martketDto = modelMapper.map(market, MarketDto.class);
        return martketDto;
    }

    public List<MarketDto> getAll() {
        List<Market> marketList = marketRepository.findAll();
        List<MarketDto> martketDtoList = marketList
                .stream()
                .map(market -> modelMapper.map(market, MarketDto.class))
                .collect(Collectors.toList());
        return martketDtoList;
    }

    public void delete(Integer id) {
        marketRepository.deleteById(id);
    }
}

