package com.example.marketapp.repository;

import com.example.marketapp.entity.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BranchRepository extends JpaRepository<Branch,Integer> {

}
