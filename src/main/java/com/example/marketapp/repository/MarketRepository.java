package com.example.marketapp.repository;

import com.example.marketapp.entity.Market;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarketRepository extends JpaRepository<Market,Integer> {
}
